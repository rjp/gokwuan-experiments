package main

import (
    "fmt"
    "sync"
    "bitbucket.org/rjp/gokwuan"
    "bitbucket.org/rjp/edf"
    "flag"
)

func displayLoginLogout(direction string, data edf.EDF_Tree) {
    username, _ := data.ValueOf("announce.username")
    fmt.Printf("%s has logged %s\n", username, direction)
}

// <announce="message_add"><messageid=2279156/><folderid=407100/><foldername="Health"/><subject="Lurgy"/><fromid=3629/><fromname="rjp"/><toid=77/><toname="David"/><replyto=2279154/><announcetime=1447448814/></>
func displayMessage(data edf.EDF_Tree) {
    dmap := data.ValueMap()
    trunc := len(dmap["subject"])
    if trunc > 32 { trunc = 32 }
    fmt.Printf("`%s` posted '%s' as [%s]/%s", dmap["fromname"], dmap["subject"][:trunc], dmap["foldername"], dmap["messageid"])
    if _, ok := dmap["replyto"]; ok {
        var toName string
        if toName, ok = dmap["toname"]; !ok {
            toName = "-unknown-"
        }
        fmt.Printf(" in reply to `%s`, %s", toName, dmap["replyto"])
    }
    fmt.Println("")
}

func handleEvents(events <-chan gokwuan.ClientEvent) {
//    fmt.Println("Waiting for events")
    for {
        event := <-events
//        fmt.Println("EVENT >", event)
        switch event.Event {
            case "announce_user_login": displayLoginLogout("in", event.Data)
            case "announce_user_logout": displayLoginLogout("out", event.Data)
            case "announce_message_add": displayMessage(event.Data)
        }
    }
}

func main() {
    var username = flag.String("u", "", "-u username")
    var password = flag.String("p", "", "-p password")
    flag.Parse()

    if *username == "" || *password == "" {
        panic("Need a username or password")
    }

    bot := gokwuan.New(*username, *password)

    // This is too much faff for a client
    // TODO change to passing the method to the initialiser?
    var wg sync.WaitGroup

    go handleEvents(bot.Events)
    wg.Add(1)

    bot.Login()

    wg.Wait()
}
