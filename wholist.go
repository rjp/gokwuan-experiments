package main

import (
    "fmt"
    "os"
    "log"
    "time"
    "flag"
    "strconv"
    "sort"
    "strings"
    "bitbucket.org/rjp/gokwuan"
    "bitbucket.org/rjp/edf"
	"github.com/nsf/termbox-go"
)

type UAClient struct {
    botChan gokwuan.AbstractChan
    outputOK chan string
    currentState string
}

func newUAClient(bc gokwuan.AbstractChan) (UAClient) {
    tmp := UAClient{bc, make(chan string), "menu"}
    return tmp
}

func displayLoginLogout(direction string, data edf.EDF_Tree) {
    username, _ := data.ValueOf("announce.username")
    fmt.Printf("%s has logged %s\r\n", username, direction)
}

// <announce="message_add"><messageid=2279156/><folderid=407100/><foldername="Health"/><subject="Lurgy"/><fromid=3629/><fromname="rjp"/><toid=77/><toname="David"/><replyto=2279154/><announcetime=1447448814/></>
func displayMessage(data edf.EDF_Tree) {
    dmap := data.ValueMap()
    trunc := len(dmap["subject"])
    if trunc > 32 { trunc = 32 }
    fmt.Printf("`%s` posted '%s' as [%s]/%s", dmap["fromname"], dmap["subject"][:trunc], dmap["foldername"], dmap["messageid"])
    if _, ok := dmap["replyto"]; ok {
        var toName string
        if toName, ok = dmap["toname"]; !ok {
            toName = "-unknown-"
        }
        fmt.Printf(" in reply to `%s`, %s", toName, dmap["replyto"])
    }
    fmt.Printf("\r\n")
}

func requestWhoList(bot gokwuan.Client) {
    defer bot.ExtraWG.Done()
    bot.ExtraWG.Add(1)
    lastRequest := time.Unix(0, 0)

    for {
        forcedRequest := false
        select {
            // If we get a blip on our "do a wholist" channel...
            case command := <- bot.Channels["wholist"]: {
                log.Printf("TRIGGERED BY %s\r\n", command.(string))
                if command == "keypress" { forcedRequest = true }
            }
            // ...or we time out after 300 seconds...
            case <- time.After(300 * time.Second):
                log.Printf("TRIGGERED BY %s\r\n", "TIMEOUT")
        }

        if forcedRequest || time.Since(lastRequest).Seconds() > 60 {
            bot.RequestWhoList()
            lastRequest = time.Now()
        }

    }
}

type whoListEntry struct {
    timeIdle int
    timeBusy int
    timeOn int
    name string
    location string
    access int
    status int
}

// By all the crikeys, this is horrible
type bySomething []whoListEntry
func (a bySomething) Len() int { return len(a) }
func (a bySomething) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

type byLoginTime struct {bySomething}
func (a byLoginTime) Less(i, j int) bool { return a.bySomething[i].timeOn < a.bySomething[j].timeOn }

type byAccessLevel struct {bySomething}
func (a byAccessLevel) Less(i, j int) bool { return a.bySomething[i].access > a.bySomething[j].access }

func qInt(s string) (int) {
    i, e := strconv.Atoi(s)
    if e != nil { i = -1 }
    return i
}

func notIdleOrBusy(w whoListEntry) bool {
    return w.timeIdle==-1 && w.timeBusy==-1
}

func showMessageList(event gokwuan.ClientEvent) {
    id, _ := event.Data.ValueOf("reply.message")
    fmt.Printf("Got a response for messageID=%s\r\n", id)
}

func formatWhoList(event gokwuan.ClientEvent) {
    wholist := []whoListEntry{}
    accesslevels := []string{"Basement", "Guest", "Messages", "Editor", "Witness", "Sysop"}

    filter := notIdleOrBusy

    for _, child := range event.Data.Children {
        if child.TypeIs("user") {
            timeIdle, e := child.ValueOf("user.login.timeidle")
            if e != nil { timeIdle = "-1" }

            timeBusy, e := child.ValueOf("user.login.timebusy")
            if e != nil { timeBusy = "-1" }

            n, e := child.ValueOf("user.name")
            // if e != nil { panic(e) }

            loginTime, e := child.ValueOf("user.login.timeon")
            if e != nil { fmt.Printf("Logged in but no timeon?\r\n") }

            location, e := child.ValueOf("user.login.location")
            if e != nil { location = "[unknown]" }

            access, e := child.ValueOf("user.accesslevel")
            if e != nil { access = "[unknown]" }

            status, e := child.ValueOf("user.login.status")
            if e != nil { status = "[unknown]" }

            tmp := whoListEntry{qInt(timeIdle), qInt(timeBusy),  qInt(loginTime), n, location, qInt(access), qInt(status)}

        if filter != nil {
            if filter(tmp) {
                wholist = append(wholist, tmp)
            }
        }
        } else {
            m := child.ValueMap()
            log.Printf("%s\r\n", m)
        }
    }


    sort.Sort(byLoginTime{wholist})
//    sort.Sort(byAccessLevel{wholist})

    // We need to know when now() is to calculate time logged in
    timeNow := time.Now()

    fmt.Printf("~~~~~~ Time ~ Access ~~ Name ~~~~~~~~~ Location ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n")
    for _, who := range wholist {

        timeOn := time.Unix(int64(who.timeOn), 0)
        d := timeNow.Sub(timeOn)
        h := int(d.Hours())
        m := int(d.Minutes()) % 60

        idleFlag, busyFlag, shadowFlag := "", "", ""
        if who.timeIdle != -1 { idleFlag = "I" }
        if who.timeBusy != -1 { busyFlag = "B" }
        if who.status & 256 == 256 { shadowFlag = "H" }

        fmt.Printf("%1s%1s%1s %4d:%02d   %-8s  %-14s %s\r\n", idleFlag, busyFlag,shadowFlag, h, m, accesslevels[who.access], who.name, who.location)
    }

    ls := "~~ " + fmt.Sprintf("Total: %d users", len(wholist)) + " "
    rs := " " + timeNow.Format("15:04:05 02/01/06") + " ~~"
    fmt.Printf("%s\r\n", ls + strings.Repeat("~", 80-len(ls)-len(rs)) + rs)
}

func handleEvents(bot gokwuan.Client) {
    defer bot.ExtraWG.Done()

    areRunning := false

//    fmt.Println("Waiting for events")
    for {
        event := <-bot.Events
//        fmt.Println("EVENT >", event.Event)
        switch event.Event {
            case "announce_user_login": bot.Channels["wholist"]<-"login"
            case "announce_user_logout": bot.Channels["wholist"]<-"logout"
            case "reply_user_list": if areRunning { formatWhoList(event) }
            case "reply_message_list": showMessageList(event)
            case "_bot_running": go requestWhoList(bot); areRunning = true; bot.Channels["wholist"]<-"do"
        }
    }
}

// func (client UAClient) termbox_looper() {
func termbox_looper(c *gokwuan.AbstractChan) {
    currentState := "menu"
loop:
	for {
        // Here we need to block on the `output-ok` channel
        // otherwise we'll be trying to read keys and output
        // menus whilst other goroutines are writing to the
        // screen
//        block := <- client.outputOK
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
            fmt.Printf("%c\r\n", ev.Ch)
            if ev.Ch == 119 || ev.Ch == 87 {
                *c <- "keypress"
            }
            if ev.Ch == 113 || ev.Ch == 81 {
                fmt.Printf("Quit?  Y/N\r\n")
                currentState = "quit-check"
                continue
            }
            if currentState == "quit-check" {
                if ev.Ch == 121 || ev.Ch == 89 {
                    fmt.Printf("Quitting!\r\n")
                    break loop
                }
                currentState = "menu"
            }
            if ev.Key == termbox.KeyCtrlQ {
                fmt.Printf("Quitting! C-Q\r\n")
                break loop
            }
		}
	}
    termbox.Close()
    os.Exit(0)
}

func main() {
    var username = flag.String("u", "", "-u username")
    var password = flag.String("p", "", "-p password")
    flag.Parse()

    if *username == "" || *password == "" {
        panic("Need a username or password")
    }

    bot := gokwuan.New(*username, *password)
    tmp_chan := make(gokwuan.AbstractChan)
    bot.Channels["wholist"] = tmp_chan

    // This should probably be some kind of abstract wrapper.
    // Having to do this in your client code is a bit rubbish.
    bot.ExtraWG.Add(1)
    go handleEvents(bot)

	err := termbox.Init()
	if err != nil {
		panic(err)
	}

	termbox.SetInputMode(termbox.InputEsc)
    go termbox_looper(&tmp_chan)

    // Never returns
    bot.Login()
}
