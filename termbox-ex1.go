package main

import (
	"fmt"
	"github.com/nsf/termbox-go"
)

// cargoculted from the keyboard demo
func main() {
	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	defer termbox.Close()

	termbox.SetInputMode(termbox.InputEsc)

loop:
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			if ev.Key == termbox.KeyCtrlQ {
				break loop
			}
			fmt.Printf("%d\r\n", ev.Ch)
		}
	}
}
